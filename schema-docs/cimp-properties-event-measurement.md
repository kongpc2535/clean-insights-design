# Event Measurement Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events
```

List of aggregated measurements of a specific event. (e.g. like a press of a button, picture taken etc.)

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## events Type

`object[]` ([Event Measurement](cimp-properties-event-measurement-event-measurement.md))
