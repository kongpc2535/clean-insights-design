# Visit Measurement Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items
```

A single aggregated measurement of repeated visits to a page/scene/activity.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## items Type

`object` ([Visit Measurement](cimp-properties-visit-measurements-visit-measurement.md))

# items Properties

| Property                      | Type      | Required | Nullable       | Defined by                                                                                                                                                                                                                                     |
| :---------------------------- | :-------- | :------- | :------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [action_name](#action_name)   | `string`  | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-visited-pagesceneactivity-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/action_name") |
| [period_start](#period_start) | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-start-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/period_start")          |
| [period_end](#period_end)     | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-end-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/period_end")              |
| [times](#times)               | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-number-of-times-occurred.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/times")                   |

## action_name

Main identifier to track page/scene/activity visits in Matomo.

`action_name`

*   is required

*   Type: `string` ([Visited Page/Scene/Activity Identifier](cimp-properties-visit-measurements-visit-measurement-properties-visited-pagesceneactivity-identifier.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-visited-pagesceneactivity-identifier.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/action_name")

### action_name Type

`string` ([Visited Page/Scene/Activity Identifier](cimp-properties-visit-measurements-visit-measurement-properties-visited-pagesceneactivity-identifier.md))

### action_name Constraints

**minimum length**: the minimum number of characters for this string is: `1`

### action_name Examples

```json
"For example, Help / Feedback will create the Action Feedback in the category Help."
```

## period_start

Beginning of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

`period_start`

*   is required

*   Type: `integer` ([Start UNIX Epoch Timestamp](cimp-properties-visit-measurements-visit-measurement-properties-start-unix-epoch-timestamp.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-start-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/period_start")

### period_start Type

`integer` ([Start UNIX Epoch Timestamp](cimp-properties-visit-measurements-visit-measurement-properties-start-unix-epoch-timestamp.md))

### period_start Examples

```json
1602499451
```

## period_end

End of the aggregation period in seconds since 1970-01-01 00:00:00 UTC

`period_end`

*   is required

*   Type: `integer` ([End UNIX Epoch Timestamp](cimp-properties-visit-measurements-visit-measurement-properties-end-unix-epoch-timestamp.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-end-unix-epoch-timestamp.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/period_end")

### period_end Type

`integer` ([End UNIX Epoch Timestamp](cimp-properties-visit-measurements-visit-measurement-properties-end-unix-epoch-timestamp.md))

### period_end Examples

```json
1602499451
```

## times

The number of times the visit to this page/scene/activity happened during the specified period.

`times`

*   is required

*   Type: `integer` ([Number of Times Occurred](cimp-properties-visit-measurements-visit-measurement-properties-number-of-times-occurred.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements-visit-measurement-properties-number-of-times-occurred.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/times")

### times Type

`integer` ([Number of Times Occurred](cimp-properties-visit-measurements-visit-measurement-properties-number-of-times-occurred.md))

### times Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### times Examples

```json
1
```

```json
2
```

```json
3
```

```json
26745
```
