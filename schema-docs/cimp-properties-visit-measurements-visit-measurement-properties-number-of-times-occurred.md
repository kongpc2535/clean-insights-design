# Number of Times Occurred Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits/items/properties/times
```

The number of times the visit to this page/scene/activity happened during the specified period.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## times Type

`integer` ([Number of Times Occurred](cimp-properties-visit-measurements-visit-measurement-properties-number-of-times-occurred.md))

## times Constraints

**minimum**: the value of this number must greater than or equal to: `1`

## times Examples

```json
1
```

```json
2
```

```json
3
```

```json
26745
```
