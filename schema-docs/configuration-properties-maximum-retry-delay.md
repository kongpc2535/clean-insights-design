# Maximum Retry Delay Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxRetryDelay
```

The SDK uses a truncated exponential backoff strategy on server failures. So the delay until it retries will rise exponentially, until it reaches `maxRetryDelay` seconds.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## maxRetryDelay Type

`number` ([Maximum Retry Delay](configuration-properties-maximum-retry-delay.md))

## maxRetryDelay Constraints

**minimum**: the value of this number must greater than or equal to: `0.1`

## maxRetryDelay Default Value

The default value is:

```json
3600
```

## maxRetryDelay Examples

```json
1800
```

```json
3600
```

```json
7200
```

```json
86400
```
