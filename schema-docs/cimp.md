# CleanInsights Matomo Proxy API Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json
```

The scheme defining the JSON API of the CleanInsights Matomo Proxy.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                             |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [cimp.schema.json](../schemas/cimp.schema.json "open original schema") |

## CleanInsights Matomo Proxy API Type

`object` ([CleanInsights Matomo Proxy API](cimp.md))

# CleanInsights Matomo Proxy API Properties

| Property          | Type      | Required | Nullable       | Defined by                                                                                                                                             |
| :---------------- | :-------- | :------- | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------- |
| [idsite](#idsite) | `integer` | Required | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-matomo-site-id.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/idsite")            |
| [lang](#lang)     | `string`  | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-http-accept-language-header.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/lang") |
| [ua](#ua)         | `string`  | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-http-user-agent-header.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/ua")        |
| [visits](#visits) | `array`   | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits")        |
| [events](#events) | `array`   | Optional | cannot be null | [CleanInsights Matomo Proxy API](cimp-properties-event-measurement.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events")         |

## idsite

The site ID used in the Matomo server which will collect and analyze the gathered data.

`idsite`

*   is required

*   Type: `integer` ([Matomo Site ID](cimp-properties-matomo-site-id.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-matomo-site-id.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/idsite")

### idsite Type

`integer` ([Matomo Site ID](cimp-properties-matomo-site-id.md))

### idsite Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### idsite Examples

```json
1
```

```json
2
```

```json
3
```

```json
345345
```

## lang

A HTTP Accept-Language header. Matomo uses this value to detect the visitor's country.

`lang`

*   is optional

*   Type: `string` ([HTTP Accept-Language Header](cimp-properties-http-accept-language-header.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-http-accept-language-header.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/lang")

### lang Type

`string` ([HTTP Accept-Language Header](cimp-properties-http-accept-language-header.md))

### lang Examples

```json
"fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5"
```

```json
"en"
```

```json
"de_AT"
```

## ua

A  HTTP User-Agent. The user agent is used to detect the operating system and browser used.

`ua`

*   is optional

*   Type: `string` ([HTTP User-Agent Header](cimp-properties-http-user-agent-header.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-http-user-agent-header.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/ua")

### ua Type

`string` ([HTTP User-Agent Header](cimp-properties-http-user-agent-header.md))

### ua Examples

```json
"Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"
```

## visits

List of aggregated measurements to specific pages/scenes/activities.

`visits`

*   is optional

*   Type: `object[]` ([Visit Measurement](cimp-properties-visit-measurements-visit-measurement.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-visit-measurements.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/visits")

### visits Type

`object[]` ([Visit Measurement](cimp-properties-visit-measurements-visit-measurement.md))

## events

List of aggregated measurements of a specific event. (e.g. like a press of a button, picture taken etc.)

`events`

*   is optional

*   Type: `object[]` ([Event Measurement](cimp-properties-event-measurement-event-measurement.md))

*   cannot be null

*   defined in: [CleanInsights Matomo Proxy API](cimp-properties-event-measurement.md "https://cleaninsights.org/schemas/cimp.schema.json#/properties/events")

### events Type

`object[]` ([Event Measurement](cimp-properties-event-measurement-event-measurement.md))
