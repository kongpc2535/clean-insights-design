# CleanInsights SDK Configuration Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json
```

The scheme defining the JSON configuration file for CleanInsights SDK.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                               |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [configuration.schema.json](../schemas/configuration.schema.json "open original schema") |

## CleanInsights SDK Configuration Type

`object` ([CleanInsights SDK Configuration](configuration.md))

# CleanInsights SDK Configuration Properties

| Property                                              | Type      | Required | Nullable       | Defined by                                                                                                                                                                                                |
| :---------------------------------------------------- | :-------- | :------- | :------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [server](#server)                                     | `string`  | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-cimp-server-address.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/server")                                       |
| [siteId](#siteid)                                     | `integer` | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-matomo-site-id.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/siteId")                                            |
| [timeout](#timeout)                                   | `number`  | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-network-timeout.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/timeout")                                          |
| [maxRetryDelay](#maxretrydelay)                       | `number`  | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-maximum-retry-delay.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxRetryDelay")                                |
| [maxAgeOfOldData](#maxageofolddata)                   | `integer` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-maximum-age-of-old-measurements.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxAgeOfOldData")                  |
| [persistEveryNTimes](#persisteveryntimes)             | `integer` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-frequency-of-data-persistence.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/persistEveryNTimes")                 |
| [serverSideAnonymousUsage](#serversideanonymoususage) | `boolean` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-override-consents-for-server-side-usage.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/serverSideAnonymousUsage") |
| [debug](#debug)                                       | `boolean` | Optional | cannot be null | [CleanInsights SDK Configuration](configuration-properties-cleaninsights-sdk-debug-flag.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/debug")                               |
| [campaigns](#campaigns)                               | `object`  | Required | cannot be null | [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns")                                  |

## server

The fully qualified URI to the CIMP (CleanInsights Matomo Proxy) endpoint, where the gathered measurements will be offloaded.

`server`

*   is required

*   Type: `string` ([CIMP Server Address](configuration-properties-cimp-server-address.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-cimp-server-address.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/server")

### server Type

`string` ([CIMP Server Address](configuration-properties-cimp-server-address.md))

### server Constraints

**URI**: the string must be a URI, according to [RFC 3986](https://tools.ietf.org/html/rfc3986 "check the specification")

### server Examples

```json
"https://matomo.example.org/ci/cleaninsights.php"
```

## siteId

The site ID used in the Matomo server which will collect and analyze the gathered data.

`siteId`

*   is required

*   Type: `integer` ([Matomo Site ID](configuration-properties-matomo-site-id.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-matomo-site-id.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/siteId")

### siteId Type

`integer` ([Matomo Site ID](configuration-properties-matomo-site-id.md))

### siteId Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### siteId Examples

```json
1
```

```json
2
```

```json
3
```

```json
345345
```

## timeout

Timeout in seconds used when trying to connect to the CIMP.

`timeout`

*   is optional

*   Type: `number` ([Network Timeout](configuration-properties-network-timeout.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-network-timeout.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/timeout")

### timeout Type

`number` ([Network Timeout](configuration-properties-network-timeout.md))

### timeout Constraints

**minimum**: the value of this number must greater than or equal to: `0.1`

### timeout Default Value

The default value is:

```json
5
```

### timeout Examples

```json
2.5
```

```json
5
```

```json
10
```

```json
30
```

## maxRetryDelay

The SDK uses a truncated exponential backoff strategy on server failures. So the delay until it retries will rise exponentially, until it reaches `maxRetryDelay` seconds.

`maxRetryDelay`

*   is optional

*   Type: `number` ([Maximum Retry Delay](configuration-properties-maximum-retry-delay.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-maximum-retry-delay.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxRetryDelay")

### maxRetryDelay Type

`number` ([Maximum Retry Delay](configuration-properties-maximum-retry-delay.md))

### maxRetryDelay Constraints

**minimum**: the value of this number must greater than or equal to: `0.1`

### maxRetryDelay Default Value

The default value is:

```json
3600
```

### maxRetryDelay Examples

```json
1800
```

```json
3600
```

```json
7200
```

```json
86400
```

## maxAgeOfOldData

The number in days of how long the SDK will try to keep sending old measurements. If the measurements become older than that, they will be purged.

`maxAgeOfOldData`

*   is optional

*   Type: `integer` ([Maximum Age of Old Measurements](configuration-properties-maximum-age-of-old-measurements.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-maximum-age-of-old-measurements.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/maxAgeOfOldData")

### maxAgeOfOldData Type

`integer` ([Maximum Age of Old Measurements](configuration-properties-maximum-age-of-old-measurements.md))

### maxAgeOfOldData Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### maxAgeOfOldData Default Value

The default value is:

```json
100
```

### maxAgeOfOldData Examples

```json
14
```

```json
30
```

```json
100
```

```json
183
```

```json
365
```

## persistEveryNTimes

Number of times of measurements after when the collected data is persisted again. Set to one to have the SDK persist the recorded data after every measurement. Set it to a very high number, to never persist. You will need to make sure, that persistence is done by you, otherwise you will loose data!

`persistEveryNTimes`

*   is optional

*   Type: `integer` ([Frequency of Data Persistence](configuration-properties-frequency-of-data-persistence.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-frequency-of-data-persistence.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/persistEveryNTimes")

### persistEveryNTimes Type

`integer` ([Frequency of Data Persistence](configuration-properties-frequency-of-data-persistence.md))

### persistEveryNTimes Constraints

**minimum**: the value of this number must greater than or equal to: `1`

### persistEveryNTimes Default Value

The default value is:

```json
10
```

### persistEveryNTimes Examples

```json
1
```

```json
10
```

```json
25
```

```json
99999999
```

## serverSideAnonymousUsage

When set to true, assumes consent for all campaigns and none for features. Only use this, when you're running on the server and don't measure anything users might need to give consent to!

`serverSideAnonymousUsage`

*   is optional

*   Type: `boolean` ([Override Consents for server-side Usage](configuration-properties-override-consents-for-server-side-usage.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-override-consents-for-server-side-usage.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/serverSideAnonymousUsage")

### serverSideAnonymousUsage Type

`boolean` ([Override Consents for server-side Usage](configuration-properties-override-consents-for-server-side-usage.md))

## debug

When set to true, the CleanInsights SDK will log a debugging information to the console.

`debug`

*   is optional

*   Type: `boolean` ([CleanInsights SDK Debug Flag](configuration-properties-cleaninsights-sdk-debug-flag.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-cleaninsights-sdk-debug-flag.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/debug")

### debug Type

`boolean` ([CleanInsights SDK Debug Flag](configuration-properties-cleaninsights-sdk-debug-flag.md))

## campaigns

Each insight you want to gain needs to be configured as a measurement campaign.

`campaigns`

*   is required

*   Type: `object` ([Measurement Campaigns](configuration-properties-measurement-campaigns.md))

*   cannot be null

*   defined in: [CleanInsights SDK Configuration](configuration-properties-measurement-campaigns.md "https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns")

### campaigns Type

`object` ([Measurement Campaigns](configuration-properties-measurement-campaigns.md))
