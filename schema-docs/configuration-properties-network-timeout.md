# Network Timeout Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/timeout
```

Timeout in seconds used when trying to connect to the CIMP.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## timeout Type

`number` ([Network Timeout](configuration-properties-network-timeout.md))

## timeout Constraints

**minimum**: the value of this number must greater than or equal to: `0.1`

## timeout Default Value

The default value is:

```json
5
```

## timeout Examples

```json
2.5
```

```json
5
```

```json
10
```

```json
30
```
