# Event Value Schema

```txt
https://cleaninsights.org/schemas/cimp.schema.json#/properties/events/items/properties/value
```

A value for the Matomo event tracking: <https://matomo.org/docs/event-tracking/>

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                              |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [cimp.schema.json*](../schemas/cimp.schema.json "open original schema") |

## value Type

`number` ([Event Value](cimp-properties-event-measurement-event-measurement-properties-event-value.md))

## value Examples

```json
0
```

```json
1
```

```json
1.5
```

```json
100
```

```json
56.44332
```
