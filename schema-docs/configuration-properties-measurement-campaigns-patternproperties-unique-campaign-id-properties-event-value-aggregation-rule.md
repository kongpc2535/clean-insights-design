# Event Value Aggregation Rule Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/eventAggregationRule
```

The rule how to aggregate the value of an event (if any given) with subsequent calls. Either 'sum' or 'avg'.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## eventAggregationRule Type

`string` ([Event Value Aggregation Rule](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-event-value-aggregation-rule.md))

## eventAggregationRule Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value   | Explanation |
| :------ | :---------- |
| `"sum"` |             |
| `"avg"` |             |

## eventAggregationRule Default Value

The default value is:

```json
"sum"
```
