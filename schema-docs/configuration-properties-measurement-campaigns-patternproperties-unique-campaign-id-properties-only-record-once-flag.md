# Only-Record-Once Flag Schema

```txt
https://cleaninsights.org/schemas/configuration.schema.json#/properties/campaigns/patternProperties/^.+$/properties/onlyRecordOnce
```

Will result in recording only the first time a visit or event happened per period. Useful for yes/no questions.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                                |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :---------------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [configuration.schema.json*](../schemas/configuration.schema.json "open original schema") |

## onlyRecordOnce Type

`boolean` ([Only-Record-Once Flag](configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id-properties-only-record-once-flag.md))
